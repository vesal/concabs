open While

let () =
  Random.self_init ();
  let res = Eval.eval (Eval.initenv) example in
  print_endline ("Real value: " ^ string_of_int (res "x"));
  let abs = Absint.eval (fun x -> (0,0)) example in
  print_endline ("Abstract value: " ^ Absint.string_of_env abs)