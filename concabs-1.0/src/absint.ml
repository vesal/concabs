open While

type domain = int * int
let string_of_domain (lo,hi) = "[" ^ string_of_int lo ^ ", " ^ string_of_int hi ^ "]"
let top = (min_int, max_int)
let bot = (max_int, min_int)
let join (lo1,hi1) (lo2,hi2) = (min lo1 lo2, max hi1 hi2)
let leq (lo1,hi1) (lo2,hi2): bool = lo2 <= lo1 && hi1 <= hi2
let sing n: domain = (n,n)

let add (lo1,hi1) (lo2,hi2) = (lo1+lo2, hi1+hi2)


type env = string -> domain
let updt env var valu: env = (function v -> if var = v then valu else env v)
let join_env e1 e2: env = (fun v -> join (e1 v) (e2 v))
let bot_env: env = (fun v -> bot)

let rec eval_aexp env aexp: domain = match aexp with 
  | Lit n -> sing n
  | Add (e1, e2) -> add (eval_aexp env e1) (eval_aexp env e2)
  | Var x -> env x

type booldomain = Bbot | Btv of bool | Btop
let leq_op (lo1,hi1) (lo2,hi2) = 
  if hi1 <= lo2 then Btv true
  else if hi2 < lo1 then Btv false
  else Btop

let rec eval_bexp env bexp: booldomain = match bexp with
  | Leq (e1, e2) -> leq_op (eval_aexp env e1) (eval_aexp env e2)
  | Tv b -> Btv b
  | Choice -> Btop

let vars = ["x"; "y"; "z"] 
let string_of_env (env: env): string = 
  String.concat ", " (List.map (fun v -> v ^ "->" ^ string_of_domain (env v)) vars)
let leq_env e1 e2 = 
  List.for_all (fun v -> leq (e1 v) (e2 v)) vars

let rec lfp (f: env -> env) (env:env): env = 
  let new_env = f env in
  if leq_env new_env env then env else lfp f new_env

let rec eval env stmt: env = match stmt with
  | Assign (v,e) -> updt env v (eval_aexp env e)
  | Seq (s1, s2) -> 
    let env1 = eval env s1 in
    eval env1 s2
  | If (b, s1, s2) -> begin
      match eval_bexp env b with
      | Btv true -> eval env s1
      | Btv false -> eval env s2
      | Btop -> join_env (eval env s1) (eval env s2)
      | Bbot -> bot_env
    end
  | While (b, s) ->
    let while_fun env: env = match eval_bexp env b with
      | Btv true -> eval env s
      | Btv false -> env
      | Btop -> join_env (eval env s) env
      | Bbot -> bot_env
    in
    lfp while_fun env
  | _ -> env