module type DOM =
sig
  type t
  val to_string: t -> string
  val top: t
  val bot: t
  val join: t -> t -> t
  val leq: t -> t -> bool
end

module BoolDom = 
struct
  type t = Bbot | Btv of bool | Btop
  let of_bool tv: t = Btv tv
  let top = Btop
  let bot = Bbot
  let join b1 b2 = match (b1,b2) with
    | (Bbot, x) | (x, Bbot) -> x
    | (Btv tv1, Btv tv2) when tv1 = tv2 -> b1
    | _ -> Btop
  let leq b1 b2: bool = match (b1,b2) with
    | (Bbot, _) | (_, Btop) -> true
    | _ -> b1 = b2
end

module IntDom = 
struct
  type t = int * int
  let to_string (lo,hi) = "[" ^ string_of_int lo ^ ", " ^ string_of_int hi ^ "]"
  let top = (min_int, max_int)
  let bot = (max_int, min_int)
  let join (lo1,hi1) (lo2,hi2) = (min lo1 lo2, max hi1 hi2)
  let leq (lo1,hi1) (lo2,hi2): bool = lo2 <= lo1 && hi1 <= hi2
  let of_int n: t = (n,n)

  (* arithmetic operations *)
  let add_op (lo1,hi1) (lo2,hi2) = (lo1+lo2, hi1+hi2)

  (* comparison operators *)
  let leq_op (lo1,hi1) (lo2,hi2): BoolDom.t = 
    if hi1 <= lo2 then Btv true
    else if hi2 < lo1 then Btv false
    else Btop

end


let vars = ["x"; "y"; "z"]

module VarMap (D: DOM) = 
struct
  module SMap = Map.Make(String)
  type t = D.t SMap.t

  let get m k: D.t = SMap.find k m
  let updt m k v: t = SMap.add k v m
  let of_fun f: t = List.fold_left (fun m v -> SMap.add v (f v) m) SMap.empty vars

  let to_string m: string = String.concat ", " (List.map (fun v -> v ^ "->" ^ D.to_string (SMap.find v m)) vars)
  let bot: t = of_fun (fun v -> D.bot)
  let top: t  = of_fun (fun v -> D.top)
  let join ht1 ht2: t = of_fun (fun v -> D.join (SMap.find v ht1) (SMap.find v ht2))
  let leq ht1 ht2: bool = List.for_all (fun v -> D.leq (SMap.find v ht1) (SMap.find v ht2)) vars
end
