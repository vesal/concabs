open While

module BD = Domains.BoolDom
module ID = Domains.IntDom
module MD = Domains.VarMap (ID)

type domain = ID.t
type booldomain = BD.t
type env = MD.t

let initenv: env = MD.of_fun (fun _ -> ID.of_int 0)
let string_of_env (env: env): string = MD.to_string env

let rec eval_aexp (env:env) aexp: domain = match aexp with 
  | Lit n -> ID.of_int n
  | Add (e1, e2) -> ID.add_op (eval_aexp env e1) (eval_aexp env e2)
  | Var x -> MD.get env x

let rec eval_bexp (env:env) bexp: booldomain = match bexp with
  | Leq (e1, e2) -> ID.leq_op (eval_aexp env e1) (eval_aexp env e2)
  | Tv b -> Btv b
  | Choice -> Btop

let cond (b: booldomain) (e1: env) (e2: env) = match b with
  | Btv true -> e1
  | Btv false -> e2
  | Btop -> MD.join e1 e2
  | Bbot -> MD.bot

let rec lfp (f: env -> env) (env:env): env = 
  let new_env = f env in
  if MD.leq new_env env then env else lfp f new_env

let rec eval (env:env) stmt: env = match stmt with
  | Assign (v,e) -> MD.updt env v (eval_aexp env e)
  | Seq (s1, s2) -> eval (eval env s1) s2
  | If (b, s1, s2) -> cond (eval_bexp env b) (eval env s1) (eval env s2)
  | While (b, s) -> lfp (fun env -> cond (eval_bexp env b) (eval env s) env) env
  | _ -> env