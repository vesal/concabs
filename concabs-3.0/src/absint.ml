open While

module ID = Domains.IntDom
module MD = Domains.VarMap (ID)
module MDB = Domains.LiftBot (MD)

type domain = ID.t
type env = MDB.t

let initenv: env = `Lift (MD.of_fun (fun _ -> ID.of_int 0))
let string_of_env (env: env): string = MDB.to_string env
let rec eval_aexp aexp (env:env): domain = match aexp with
  | Var v -> MDB.to_t (MDB.lift (MD.get v) env)
  | Intv (lo, hi) -> (lo, hi)
  | Add (e1, e2) -> ID.add_op (eval_aexp e1 env) (eval_aexp e2 env)

let eval_assign v e (env:env): env =
  if env = MDB.bot then (* avoid eager eval_aexp on bot *)
    MDB.bot
  else
    MDB.lift (MD.updt v (eval_aexp e env)) env

let eval_guard b tv (env:env): env =
  if env = MDB.bot then
    MDB.bot
  else
    match b, tv with
    | Leq0 e, true -> (* e <= 0 *)
      let (loe, hie) = eval_aexp e env in
      if loe <= 0 then match e with
        | Var x -> MDB.lift (MD.updt x (loe, 0)) env
        | _ -> env
      else
        MDB.bot
    | Leq0 e, false -> (* !(e <= 0) => e > 0 *)
      let (loe, hie) = eval_aexp e env in
      if hie > 0 then match e with
        | Var x -> MDB.lift (MD.updt x (1, hie)) env
        | _ -> env
      else
        MDB.bot

let rec lfp (f: env -> env) (env:env): env = 
  let new_env = f env in
  if MDB.leq new_env env then env else lfp f new_env

let rec lim (f: env -> env) (env:env): env =
  let new_env = f env in
  let widened = MDB.widen env new_env in
  if MDB.leq widened env then env else lim f widened

let rec eval stmt (env:env): env = match stmt with
  | Assign (v, e) -> eval_assign v e env
  | Seq (s1, s2) -> eval s2 (eval s1 env)
  | If (b, s) -> MDB.join (eval s (eval_guard b true env)) (eval_guard b false env)
  | While (b, s) -> 
    let f envX = MDB.join env (eval s (eval_guard b true envX)) in
    eval_guard b false (lim f MDB.bot)
  | Nop -> env