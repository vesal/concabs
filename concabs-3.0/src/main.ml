open While

let example = ex08

let () =
  let abs = Absint.eval example Absint.initenv in
  print_endline ("Abstract value: " ^ Absint.string_of_env abs)

(* let () =
  print_endline (Absint.MDB.(to_string (widen bot Absint.initenv))) *)

(* let () =
  print_endline (Domains.IntDom.(to_string (widen bot (0, 0)))) *)