type aexp = 
  | Var of string
  | Intv of int * int
  | Add of aexp * aexp
  (* other aexp operators? *)

type bexp = 
  | Leq0 of aexp
  (* other bexp operators? *)

type stmt = 
  | Assign of string * aexp    (* v := e *)
  | If of bexp * stmt          (* if b then s *)
  | While of bexp * stmt       (* while b do s *)
  | Seq of stmt * stmt         (* s1; s2 *)
  | Nop                        (* ; *)


let lit v = Intv (v, v)
let choose = Leq0 (Intv (0, 1))

let ex01 = Assign ("x", Add (lit 3, lit 10))

let ex02 = Seq (Assign ("y", lit 5), Assign ("x", Add (lit 2, Var "y")))

let ex03 = If (Leq0 (lit 0), ex01)
let ex03b = If (Leq0 (lit 1), ex01)

let ex04 = If (choose, ex01)

let ex05a = While (Leq0 (lit 1), Nop)
let ex05b = While (Leq0 (lit 0), Nop)
let ex05 =
  Seq (
    Assign ("y", lit (-10)),
    While (Leq0 (Var "y"),
      Seq (
        Assign ("x", Add (Var "x", Var "y")),
        Assign ("y", Add (Var "y", lit 1))
      )
    )
  )

let ex08a =
  If (choose,
    Assign ("y", Add (Var "y", lit 1))
  )
let ex08 =
  Seq (
    ex08a,
    If (Leq0 (Var "y"),
      Assign ("y", Add (Var "y", lit 1))
    )
  )