module type DOM =
sig
  type t
  val to_string: t -> string
  (* val top: t *) (* unneeded *)
  val bot: t
  val join: t -> t -> t
  val leq: t -> t -> bool
  val widen: t -> t -> t
  (* val narrow: t -> t -> t *) (* unneeded *)
end

(* unused *)
module BoolDom = 
struct
  type t = Bbot | Btv of bool | Btop
  let of_bool tv: t = Btv tv
  let top = Btop
  let bot = Bbot
  let join b1 b2 = match (b1,b2) with
    | (Bbot, x) | (x, Bbot) -> x
    | (Btv tv1, Btv tv2) when tv1 = tv2 -> b1
    | _ -> Btop
  let leq b1 b2: bool = match (b1,b2) with
    | (Bbot, _) | (_, Btop) -> true
    | _ -> b1 = b2
  let widen b1 b2 = b2
  let narrow b1 b2 = b2
end

module IntDom = 
struct
  type t = int * int

  let bot = (max_int, min_int) (* hacky bot, safer to lift bot *)
  let to_string (lo,hi) =
    if (lo, hi) = bot then
      "[bot]"
    else
      let s i =
        if i = min_int then
          "-inf"
        else if i = max_int then
          "inf"
        else
          string_of_int i
      in
      "[" ^ s lo ^ ", " ^ s hi ^ "]"
  let top = (min_int, max_int)
  let join (lo1,hi1) (lo2,hi2) = (min lo1 lo2, max hi1 hi2)
  let leq (lo1,hi1) (lo2,hi2): bool = lo2 <= lo1 && hi1 <= hi2
  let widen (lo1,hi1) (lo2,hi2): t =
    if (lo1, hi1) <> bot then (
      (if lo2 < lo1 then min_int else lo1),
      (if hi1 < hi2 then max_int else hi2))
    else (* unneeded with proper (lift)bot *)
      (lo2, hi2)

  let narrow (lo1,hi1) (lo2,hi2): t = (
    (if lo1 = min_int then lo2 else lo1), 
    (if hi1 = max_int then hi2 else hi1))
  let of_int n: t = (n,n)

  (* arithmetic operations *)
  let add_op (lo1,hi1) (lo2,hi2) =
    (* TODO: check for overflow *)
    let lo = if lo1 = min_int || lo2 = min_int then min_int else lo1 + lo2 in
    let hi = if hi1 = max_int || hi2 = max_int then max_int else hi1 + hi2 in
    (lo, hi)

  (* comparison operators *)
  let leq_op (lo1,hi1) (lo2,hi2): BoolDom.t = 
    if hi1 <= lo2 then Btv true
    else if hi2 < lo1 then Btv false
    else Btop

end

module type MAP =
sig
  include DOM
  type key
  type value
  val get: key -> t -> value
  val updt: key -> value -> t -> t
  val of_pairs: (key * value) list -> t
end

module MapDom (K: Keys.Key) (D: DOM) =
struct
  module KMap = Map.Make (K)
  type t = D.t KMap.t
  type key = K.t
  type value = D.t

  let get_or_bot = function
    | None -> D.bot
    | Some v -> v

  let get k m: D.t = get_or_bot (KMap.find_opt k m)

  let updt k v m: t = KMap.add k v m
  let of_pairs ps: t = List.fold_left (fun acc (k, v) -> KMap.add k v acc) KMap.empty ps
  let of_pairs_merge ps: t = List.fold_left (fun acc (k, v) -> KMap.update k (function | None -> Some v | Some pv -> Some (D.join pv v)) acc) KMap.empty ps
  let to_pairs m = KMap.bindings m

  let to_string m = "{" ^ KMap.fold (fun k v acc ->
      if v = D.bot then (* don't print useless bot values, as good as missing, possibly unneeded now *)
        acc
      else
        (if acc = "" then acc else acc ^ ", ") ^ K.to_string k ^ "->" ^ D.to_string v
    ) m "" ^ "}"
  let bot = KMap.empty
  let join = KMap.merge (fun k v1 v2 -> Some (D.join (get_or_bot v1) (get_or_bot v2)))
  let widen = KMap.merge (fun k v1 v2 -> Some (D.widen (get_or_bot v1) (get_or_bot v2)))
  let leq m1 m2 = KMap.for_all (fun k v1 -> D.leq v1 (get k m2)) m1
end

module VarMap (D: DOM) = MapDom (Keys.String) (D)


module LiftBot (D: DOM) =
struct
  type t = [
    | `Bot
    | `Lift of D.t
  ]

  let to_string = function
    | `Bot -> "bot"
    | `Lift x -> "lift " ^ D.to_string x
  (* let top = `Lift D.top *)
  let bot = `Bot
  let join x y = match x, y with
    | `Lift x, `Lift y -> `Lift (D.join x y)
    | `Bot, x | x, `Bot -> x
  let leq x y = match x, y with
    | `Bot, _ -> true
    | _, `Bot -> false
    | `Lift x, `Lift y -> D.leq x y
  let widen x y = match x, y with
    | `Lift x, `Lift y -> `Lift (D.widen x y)
    | `Bot, x | x, `Bot -> x
  (* let narrow x y = match x, y with
    | `Bot, x | x, `Bot -> `Bot (* is this right? *)
    | `Lift x, `Lift y -> `Lift (D.narrow x y) *)

  let map f = function
    | `Bot -> `Bot
    | `Lift x -> `Lift (f x)
  let map2 f x y = match x, y with
    | `Lift x, `Lift y -> `Lift (f x y)
    | `Bot, _ | _, `Bot -> `Bot
  let flat_map f = function
    | `Bot -> `Bot
    | `Lift x -> f x

  let unlift = function
    | `Bot -> failwith "unlift: bot"
    | `Lift x -> x
end

module LiftBotMap (M: MAP) =
struct
  include LiftBot (M)
  type key = M.key
  type value = M.value

  let get k = map (M.get k)
  let updt k v = map (M.updt k v)
  let of_pairs ps = `Lift (M.of_pairs ps)
end

module Prod (D1: DOM) (D2: DOM) =
struct
  type t = D1.t * D2.t

  let to_string (x1, x2) = "(" ^ D1.to_string x1 ^ ", " ^ D2.to_string x2 ^ ")"
  (* let top = (D1.top, D2.top) *)
  let bot = (D1.bot, D2.bot)
  let join (x1, x2) (y1, y2) = (D1.join x1 y1, D2.join x2 y2)
  let leq (x1, x2) (y1, y2) = D1.leq x1 y1 && D2.leq x2 y2
  let widen (x1, x2) (y1, y2) = (D1.widen x1 y1, D2.widen x2 y2)
  (* let narrow (x1, x2) (y1, y2) = (D1.narrow x1 y1, D2.narrow x2 y2) *)
end