open While

module MutexSet =
struct
  include Set.Make (String)
  let to_string s = "{" ^ fold (fun x acc -> (if acc = "" then acc else acc ^ ", ") ^ x) s "" ^ "}"

  let disjoint s t = is_empty (inter s t)
  let contains x s = match find_opt x s with
    | None -> false
    | Some _ -> true
  let to_list s = fold (fun x acc -> x :: acc) s []
end

type comm =
  | Weak
  | Sync of mutex
module CommKey =
struct
  type t = comm
  let compare = Pervasives.compare
  let to_string = function
    | Weak -> "weak"
    | Sync m -> "sync(" ^ m ^ ")"
end
module ConfKey = Keys.Prod3 (MutexSet) (MutexSet) (CommKey) (* u component actually unused because no IsLocked, Yield support *)
module IntfKey = Keys.Prod3 (Keys.Int) (ConfKey) (Keys.String)

module ID = Domains.IntDom
module IDB = Domains.LiftBot (ID)
module MD = Domains.VarMap (ID)
module MDB = Domains.LiftBotMap (MD)
module IntfD = Domains.MapDom (IntfKey) (IDB)
module EnvsD = Domains.MapDom (ConfKey) (MDB) (* s component always Weak by convention, could be omitted *)
module DD = Domains.Prod (EnvsD) (IntfD)

type env = MDB.t
type envs = EnvsD.t
type conf = ConfKey.t
type intf = IntfD.t
type domain = DD.t

let vars = ["x"; "y"; "z"; "w"]
let list_flat_map f l = List.flatten (List.map f l)
let initenv: env =
  vars
  |> List.map (fun k -> (k, ID.of_int 0))
  |> MDB.of_pairs
let initenvs: envs = EnvsD.of_pairs [((MutexSet.empty, MutexSet.empty, Weak), initenv)]
let string_of_domain (domain:domain): string = DD.to_string domain
let rec eval_aexp aexp (env:env): IDB.t = match aexp with
  | Var v -> MDB.get v env
  | Intv (lo, hi) -> `Lift (lo, hi)
  | Add (e1, e2) -> IDB.map2 ID.add_op (eval_aexp e1 env) (eval_aexp e2 env)

let get v (env:env): IDB.t = MDB.get v env

let as_expr (av:ID.t): aexp =
  let (l, h) = av in
  Intv (l, h)

let rec subst f e = match e with
  | Var v -> f v
  | Intv (lo, hi) -> Intv (lo, hi)
  | Add (e1, e2) -> Add (subst f e1, subst f e2)

let intf_weak (l, u, s) (l', u', s') =
  MutexSet.disjoint l l'
  && MutexSet.disjoint u l'
  && MutexSet.disjoint u' l
  && s = Weak
  && s' = Weak

let apply t (c:conf) (envs:envs) (intf:intf) e =
  let env = EnvsD.get c envs in
  let module YD = Domains.VarMap (IDB) in
  (* use VarMap to groupby calculate all variable intfs with one pass *)
  let y_intfs =
    IntfD.to_pairs intf
    |> List.filter (fun ((t', c', y), y_intf) -> t <> t' && intf_weak c c')
    |> List.map (fun ((t', c', y), y_intf) -> (y, y_intf))
    |> YD.of_pairs_merge
  in
  (* TODO: compute substitution exprs only once per variable *)
  let f y =
    let y_intf = YD.get y y_intfs in
    if y_intf = IDB.bot then
      Var y
    else
      as_expr (IDB.unlift (IDB.join y_intf (get y env))) (* safe unlift since v_intf <> bot *)
  in
  subst f e

let eval_assign1 v e (env:env): env =
  eval_aexp e env
  |> IDB.flat_map (fun x -> MDB.updt v x env)

let eval_assign v e t (envs, intf): domain =
  let new_envs =
    EnvsD.to_pairs envs
    |> List.map (fun (c, env) ->
      (c, eval_assign1 v (apply t c envs intf e) env)
    )
    |> EnvsD.of_pairs_merge
  in
  let new_intf =
    EnvsD.to_pairs new_envs
    |> List.fold_left (fun intf (c, new_env) ->
      IntfD.updt (t, c, v) (IDB.join (IntfD.get (t, c, v) intf) (get v new_env)) intf
    ) intf
  in
  (new_envs, new_intf)

let eval_guard1 b tv (env:env): env = match b with
  | Leq0 e ->
    eval_aexp e env
    |> IDB.flat_map (fun (loe, hie) ->
      match tv with
      | true when loe <= 0 -> begin
        match e with
        | Var x -> MDB.updt x (loe, 0) env
        | _ -> env
        end
      | false when hie > 0 -> begin
        match e with
        | Var x -> MDB.updt x (1, hie) env
        | _ -> env
        end
      | _ -> MDB.bot (* dead code *)
    )

let eval_guard b tv t (envs, intf): domain = match b with
  | Leq0 e ->
    let new_envs =
      EnvsD.to_pairs envs
      |> List.map (fun (c, env) ->
        (c, eval_guard1 (Leq0 (apply t c envs intf e)) tv env)
      )
      |> EnvsD.of_pairs_merge
    in
    (new_envs, intf)

let in_mutex t l u m env intf =
  IntfD.to_pairs intf
  |> List.filter (fun ((t', (l', u', s'), x), x_intf) ->
    t <> t' && MutexSet.disjoint l l' && MutexSet.disjoint l u' && MutexSet.disjoint l' u && s' = Sync m
  )
  |> List.map (fun ((t', (l', u', s'), x), x_intf) ->
    (* eval_assign1 x (as_expr (IDB.unlift xx)) env (* why safe unlift? *) *)
    (* bot x_intf assumed to be impossible in Miné? *)
    IDB.flat_map (fun x_intf -> eval_assign1 x (as_expr x_intf) env) x_intf
  )
  |> List.fold_left MDB.join MDB.bot
  |> MDB.join env

let out_mutex t l u m env intf =
  IntfD.to_pairs intf
  |> List.filter (fun ((t', (l', _, s'), x), x_intf) ->
    t = t' && MutexSet.contains m l' && s' = Weak && x_intf <> IDB.bot
  )
  |> List.map (fun ((t', (l', _, s'), x), x_intf) -> x)
  (* TODO: remove duplicate x-s, can happen at all? *)
  |> List.map (fun x ->
    let t' = t in
    let c = (l, u, Sync m) in
    ((t', c, x),
      get x env
    )
  )
  |> IntfD.of_pairs

let eval_lock m t (envs, intf): domain =
  (
    EnvsD.to_pairs envs
    |> List.filter (fun ((l', u', s), env) -> s = Weak)
    |> List.map (fun ((l', u', s), env) ->
      let l = MutexSet.add m l' in
      let u = MutexSet.empty in
      ((l, u, s),
        in_mutex t l' MutexSet.empty m env intf
      )
    )
    |> EnvsD.of_pairs_merge
  ,
    EnvsD.to_pairs envs
    |> List.filter (fun ((l', u', s), env) -> s = Weak)
    |> list_flat_map (fun ((l, u, s), env) ->
      List.map (fun m' ->
        out_mutex t l MutexSet.empty m' env intf
      ) (MutexSet.to_list u)
    )
    |> List.fold_left IntfD.join IntfD.bot
    |> IntfD.join intf
  )

let eval_unlock m t (envs, intf): domain =
  (
    EnvsD.to_pairs envs
    |> List.filter (fun ((l', u', s), env) -> s = Weak)
    |> List.map (fun ((l', u', s), env) ->
      let l = MutexSet.remove m l' in
      let u = u' in
      ((l, u, s),
        env
      )
    )
    |> EnvsD.of_pairs_merge
  ,
    EnvsD.to_pairs envs
    |> List.filter (fun ((l', u', s), env) -> s = Weak)
    |> List.map (fun ((l, u, s), env) ->
      out_mutex t (MutexSet.remove m l) u m env intf
    )
    |> List.fold_left IntfD.join IntfD.bot
    |> IntfD.join intf
  )

module Fixpoint (D: Domains.DOM) =
struct
  let rec lfp' (f: D.t -> D.t) (x:D.t): D.t =
    let new_x = f x in
    if D.leq new_x x then x else lfp' f new_x
  let lfp f = lfp' f D.bot

  let rec lim' (f: D.t -> D.t) (x:D.t): D.t =
    let new_x = f x in
    let widened = D.widen x new_x in
    (* print_endline ("x   " ^ D.to_string x);
    print_endline ("f x " ^ D.to_string new_x);
    print_endline ("wid " ^ D.to_string widened);
    prerr_endline ""; *)
    if D.leq widened x then x else lim' f widened
  let lim f = lim' f D.bot
end

module DDFP = Fixpoint (DD)
let rec eval_thread stmt t (domain:domain): domain = match stmt with
  | Assign (v, e) -> eval_assign v e t domain
  | Seq (s1, s2) -> eval_thread s2 t (eval_thread s1 t domain)
  | If (b, s) -> DD.join (eval_thread s t (eval_guard b true t domain)) (eval_guard b false t domain)
  | While (b, s) -> 
    let f envX = DD.join domain (eval_thread s t (eval_guard b true t envX)) in
    eval_guard b false t (DDFP.lim f)
  | Nop -> domain
  | Lock m -> eval_lock m t domain
  | Unlock m -> eval_unlock m t domain

module IntfDFP = Fixpoint (IntfD)
let rec eval (threads: stmt list) (envs:envs): domain =
  let f' intf =
    let ret = List.mapi (fun t thread ->
      let ret = eval_thread thread t (envs, intf) in
      print_endline ("thread " ^ string_of_int t ^ ": " ^ DD.to_string ret);
      ret
    ) threads
    |> List.fold_left DD.join DD.bot
    in
    print_endline "";
    ret
  in
  let f intf = snd (f' intf) in
  f' (IntfDFP.lim f) (* extra f' to compute envs after last intfs *)
  (* joined envs are slightly meaningless, single thread env from last iteration is more accurate *)
  (* could consider extra reader thread of all vars -- completely(?) ignores locking? *)
  (* could consider each thread with own lock and reader reading all vars sync -- maybe not useful for infinite loops? *)
  (* extracting data races (write-write, write-read) from intfs might still be the best information for comparison *)

  (* final env overapproximates from initial env *)
  (* par_ex01: x->[0, 1] not x->[1, 1] because x->[0, 0] joined from thread 1 initial env *)
  (* par_ex_fig9b: y->[0, inf] not y->[1, inf] because y->[0, 0] joined from thread 1 initial env *)