type aexp = 
  | Var of string
  | Intv of int * int
  | Add of aexp * aexp
  (* other aexp operators? *)

type bexp = 
  | Leq0 of aexp
  (* other bexp operators? *)

type mutex = string

type stmt = 
  | Assign of string * aexp    (* v := e *)
  | If of bexp * stmt          (* if b then s *)
  | While of bexp * stmt       (* while b do s *)
  | Seq of stmt * stmt         (* s1; s2 *)
  | Lock of mutex              (* lock(m) *)
  | Unlock of mutex            (* unlock(m) *)
(*  | IsLocked of string * mutex (* v := islocked(m) *)
  | Yield                      (* yield *) *)
  | Nop                        (* ; *)


let lit v = Intv (v, v)
let choose = Leq0 (Intv (0, 1))
let seq = List.fold_left (fun acc s -> Seq (acc, s)) Nop

let ex01 = Assign ("x", Add (lit 3, lit 10))

let ex02 = Seq (Assign ("y", lit 5), Assign ("x", Add (lit 2, Var "y")))

let ex03 = If (Leq0 (lit 0), ex01)
let ex03b = If (Leq0 (lit 1), ex01)

let ex04 = If (choose, ex01)

let ex05a = While (Leq0 (lit 1), Nop)
let ex05b = While (Leq0 (lit 0), Nop)
let ex05 =
  Seq (
    Assign ("y", lit (-10)),
    While (Leq0 (Var "y"),
      Seq (
        Assign ("x", Add (Var "x", Var "y")),
        Assign ("y", Add (Var "y", lit 1))
      )
    )
  )

let ex08a =
  If (choose,
    Assign ("y", Add (Var "y", lit 1))
  )
let ex08 =
  Seq (
    ex08a,
    If (Leq0 (Var "y"),
      Assign ("y", Add (Var "y", lit 1))
    )
  )


let par_ex01 = [
  Assign ("x", lit 1)
  ;
  Assign ("y", Var "x")
]

let par_ex02 = [
  Assign ("x", Add (Var "x", lit 1))
  ;
  Assign ("x", Add (Var "x", lit 1))
]

let par_ex_fig9b = [
  Seq (
    Assign ("x", Add (Var "x", lit 1)),
    Assign ("y", Var "x")
  )
  ;
  Assign ("x", Add (Var "x", lit 1))
]

let par_ex03 = [
  seq [
    Lock "m";
    Assign ("x", lit 10); (* should never be seen by thread 1 *)
    Assign ("x", lit 5);
    Unlock "m"
  ]
  ;
  seq [
    Lock "m";
    Assign ("y", Var "x");
    Unlock "m"
  ]
]

let par_ex_fig16a = [
  seq [
    Lock "m";
    Assign ("x", lit 15);
    Assign ("x", lit 10);
    Unlock "m"
  ]
  ;
  seq [
    Lock "m";
    Assign ("y", Var "x"); (* sees 10 *)
    Assign ("x", lit 5);
    Assign ("z", Var "x"); (* sees 5 *)
    Unlock "m"
  ]
]

let par_ex_fig16b = [
  seq [
    Lock "m";
    Assign ("x", lit 15);
    Assign ("x", lit 10);
    Unlock "m";
    Assign ("x", lit (-5))
  ]
  ;
  seq [
    Assign ("y", Var "x"); (* sees 15, 10, -5 *)
    Lock "m";
    Assign ("z", Var "x"); (* sees 10, -5 *)
    Assign ("x", lit 5);
    Assign ("w", Var "x"); (* sees 5, -5 *)
    Unlock "m"
  ]
]

let par_ex_fig17ab = [
  seq [
    Lock "m1";
    Assign ("x", lit 15);
    Lock "m2";
    Assign ("x", lit 10);
    Unlock "m1";
    (* Assign ("x", lit (-10)); *)
    Assign ("x", lit (-5));
    Unlock "m2"
  ]
  ;
  seq [
    Assign ("y", Var "x"); (* sees 15, 10, -5 *)
    Lock "m1";
    Assign ("z", Var "x"); (* sees 10, -5 *)
    Lock "m2";
    Assign ("w", Var "x")  (* sees 10, -5 -- -5 from sync(m2) intf, 10 from prev env join in in_mutex *)
  ]
]

let par_ex_fig17c = [
  seq [
    Lock "m1";
    Assign ("x", lit 15);
    Lock "m2";
    Assign ("x", lit 10);
    Unlock "m2";
    Assign ("x", lit (-5));
    Unlock "m1"
  ]
  ;
  seq [
    Assign ("y", Var "x"); (* sees 15, 10, -5 *)
    Lock "m1";
    Assign ("z", Var "x"); (* sees -5 *)
    Lock "m2";
    Assign ("w", Var "x")  (* sees -5 *)
  ]
]