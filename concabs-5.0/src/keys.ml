module type Key =
sig
  include Map.OrderedType
  val to_string: t -> string
end


module String =
struct
  include String
  let to_string s = s
end

module Int =
struct
  type t = int
  let compare = Pervasives.compare
  let to_string = string_of_int
end

module Prod (K1: Key) (K2: Key) =
struct
  type t = K1.t * K2.t
  let compare (x1, x2) (y1, y2) =
    let c1 = K1.compare x1 y1 in
    if c1 <> 0 then c1 else
      let c2 = K2.compare x2 y2 in
      c2
  let to_string (x1, x2) = "(" ^ K1.to_string x1 ^ ", " ^ K2.to_string x2 ^ ")"
end

module Prod3 (K1: Key) (K2: Key) (K3: Key) =
struct
  type t = K1.t * K2.t * K3.t
  let compare (x1, x2, x3) (y1, y2, y3) =
    let c1 = K1.compare x1 y1 in
    if c1 <> 0 then c1 else
      let c2 = K2.compare x2 y2 in
      if c2 <> 0 then c2 else
        let c3 = K3.compare x3 y3 in
        c3
  let to_string (x1, x2, x3) = "(" ^ K1.to_string x1 ^ ", " ^ K2.to_string x2 ^ ", " ^ K3.to_string x3 ^ ")"
end