open While

module MutexSet =
struct
  include Set.Make (String)
  let to_string s = "{" ^ fold (fun x acc -> (if acc = "" then acc else acc ^ ", ") ^ x) s "" ^ "}"

  let disjoint s t = is_empty (inter s t)
  let contains x s = match find_opt x s with
    | None -> false
    | Some _ -> true
  let to_list s = fold (fun x acc -> x :: acc) s []
end

type comm =
  | Weak
  | Sync of mutex
module CommKey =
struct
  type t = comm
  let compare = Pervasives.compare
  let to_string = function
    | Weak -> "weak"
    | Sync m -> "sync(" ^ m ^ ")"
end
module ConfKey = Keys.Prod3 (MutexSet) (MutexSet) (CommKey)
module IntfKey = Keys.Prod3 (Keys.Int) (ConfKey) (Keys.String)

module ID = Domains.IntDom
module IDB = Domains.LiftBot (ID)
module MD = Domains.VarMap (ID)
module MDB = Domains.LiftBotMap (MD)
module IntfD = Domains.MapDom (IntfKey) (IDB)
module EnvsD = Domains.MapDom (ConfKey) (MDB)
module DD = Domains.Prod (EnvsD) (IntfD)

type env = MDB.t
type envs = EnvsD.t
type conf = ConfKey.t
type intf = IntfD.t
type domain = DD.t

let vars = ["x"; "y"; "z"; "w"]
let mutexes = ["m"; "m1"; "m2"]
(* let mutexes = ["m"] *)
(* let mutexes = ["m1"; "m2"] *)
let comms = Weak :: List.map (fun m -> Sync m) mutexes
let rec powerset = function (* https://stackoverflow.com/a/40143586 *)
  | [] -> [[]]
  | x :: xs ->
    let ps = powerset xs in
    ps @ List.map (fun ss -> x :: ss) ps
let list_prod l1 l2 = List.concat (List.map (fun x1 -> List.map (fun x2 -> (x1, x2)) l2) l1)
let list_flat_map f l = List.flatten (List.map f l)
let mutex_sets = List.map (MutexSet.of_list) (powerset mutexes)
let all_lus = list_prod mutex_sets mutex_sets
let all_confs =
  list_prod all_lus comms
  |> List.map (fun ((l, u), s) -> (l, u, s))
let initenv: env =
  vars
  |> List.map (fun k -> (k, ID.of_int 0))
  |> MDB.of_pairs
let initenvs: envs = EnvsD.of_pairs [((MutexSet.empty, MutexSet.empty, Weak), initenv)]
let string_of_domain (domain:domain): string = DD.to_string domain
let rec eval_aexp aexp (env:env): IDB.t = match aexp with
  | Var v -> MDB.get v env
  | Intv (lo, hi) -> `Lift (lo, hi)
  | Add (e1, e2) -> IDB.map2 ID.add_op (eval_aexp e1 env) (eval_aexp e2 env)

let get v (env:env): IDB.t = MDB.get v env

let as_expr (av:ID.t): aexp =
  let (l, h) = av in
  Intv (l, h)

let rec subst f e = match e with
  | Var v -> f v
  | Intv (lo, hi) -> Intv (lo, hi)
  | Add (e1, e2) -> Add (subst f e1, subst f e2)

let intf_weak (l, u, s) (l', u', s') =
  MutexSet.disjoint l l'
  && MutexSet.disjoint u l'
  && MutexSet.disjoint u' l
  && s = Weak
  && s' = Weak

let apply threads t (c:conf) (envs:envs) (intf:intf) e =
  let env = EnvsD.get c envs in
  let f v =
    let other_ts = List.filter (fun t2 -> t2 <> t) threads in
    let intf_confs = List.filter (intf_weak c) all_confs in
    let v_intf =
      list_prod other_ts intf_confs
      |> List.map (fun (t2, c2) -> IntfD.get (t2, c2, v) intf)
      |> List.fold_left IDB.join IDB.bot in
    if v_intf = IDB.bot then
      Var v
    else
      as_expr (IDB.unlift (IDB.join v_intf (get v env))) (* safe unlift since v_intf <> bot *)
  in
  subst f e

let eval_assign1 v e (env:env): env =
  eval_aexp e env
  |> IDB.flat_map (fun x -> MDB.updt v x env)

let eval_assign threads v e t (envs, intf): domain =
  let new_envs = EnvsD.of_pairs (List.map (fun (l, u) -> let c = (l, u, Weak) in let env = EnvsD.get c envs in (c, eval_assign1 v (apply threads t c envs intf e) env)) all_lus) in
  let new_intf = List.fold_left (fun intf c -> let new_env = EnvsD.get c new_envs in IntfD.updt (t, c, v) (IDB.join (IntfD.get (t, c, v) intf) (get v new_env)) intf) intf all_confs in
  (new_envs, new_intf)

let eval_guard1 b tv (env:env): env = match b with
  | Leq0 e ->
    eval_aexp e env
    |> IDB.flat_map (fun (loe, hie) ->
      match tv with
      | true when loe <= 0 -> begin
        match e with
        | Var x -> MDB.updt x (loe, 0) env
        | _ -> env
        end
      | false when hie > 0 -> begin
        match e with
        | Var x -> MDB.updt x (1, hie) env
        | _ -> env
        end
      | _ -> MDB.bot
    )

let eval_guard threads b tv t (envs, intf): domain = match b with
  | Leq0 e ->
    let new_envs = EnvsD.of_pairs (List.map (fun (l, u) -> let c = (l, u, Weak) in let env = EnvsD.get c envs in (c, eval_guard1 (Leq0 (apply threads t c envs intf e)) tv env)) all_lus) in
    (new_envs, intf)

let in_mutex threads t l u m env intf =
  let other_ts = List.filter (fun t2 -> t2 <> t) threads in
  let lus = List.filter (fun (l', u') -> MutexSet.disjoint l l' && MutexSet.disjoint l u' && MutexSet.disjoint l' u) all_lus in
  list_flat_map (fun x ->
    list_flat_map (fun t' ->
      List.map (fun (l', u') ->
        let xx = IntfD.get (t', (l', u', Sync m), x) intf in
        (* eval_assign1 x (as_expr (IDB.unlift xx)) env (* why safe unlift? *) *)
        IDB.flat_map (fun xx -> eval_assign1 x (as_expr xx) env) xx
      ) lus
    ) other_ts
  ) vars
  |> List.fold_left MDB.join MDB.bot
  |> MDB.join env

let out_mutex threads t l u m env intf =
  IntfD.of_pairs (
    list_flat_map (fun t' ->
      list_flat_map (fun c ->
        List.map (fun x ->
          ((t', c, x),
            if t = t' && c = (l, u, Sync m) && List.exists (fun c' -> let (l', _, s') = c' in s' = Weak && MutexSet.contains m l' && IntfD.get (t, c', x) intf <> IDB.bot) all_confs then
              get x env
            else
              IDB.bot
          )
        ) vars
      ) all_confs
    ) threads
  )

let eval_lock threads m t (envs, intf): domain =
  (
    EnvsD.of_pairs (List.map (fun (l, u) ->
      ((l, u, Weak),
        (
          if MutexSet.contains m l && MutexSet.is_empty u then
            List.append (
              let l' = MutexSet.remove m l in
              List.map (fun u' ->
                in_mutex threads t l' MutexSet.empty m (EnvsD.get (l', u', Weak) envs) intf
              ) mutex_sets
            ) (
              let l' = l in
              List.map (fun u' ->
                in_mutex threads t l' MutexSet.empty m (EnvsD.get (l', u', Weak) envs) intf
              ) mutex_sets
            )
          else
            []
        )
        |> List.fold_left MDB.join MDB.bot
      )
    ) all_lus)
  ,
    list_flat_map (fun (l, u) ->
      List.map (fun m' ->
        out_mutex threads t l MutexSet.empty m' (EnvsD.get (l, u, Weak) envs) intf
      ) (MutexSet.to_list u)
    ) all_lus
    |> List.fold_left IntfD.join IntfD.bot
    |> IntfD.join intf
  )

let eval_unlock threads m t (envs, intf): domain =
  (
    EnvsD.of_pairs (List.map (fun (l, u) ->
      ((l, u, Weak),
        if MutexSet.contains m l then
          MDB.bot
        else
          MDB.join (EnvsD.get (l, u, Weak) envs) (EnvsD.get (MutexSet.add m l, u, Weak) envs)
      )
    ) all_lus)
  ,
    List.map (fun (l, u) ->
      out_mutex threads t (MutexSet.remove m l) u m (EnvsD.get (l, u, Weak) envs) intf
    ) all_lus
    |> List.fold_left IntfD.join IntfD.bot
    |> IntfD.join intf
  )

module Fixpoint (D: Domains.DOM) =
struct
  let rec lfp' (f: D.t -> D.t) (x:D.t): D.t =
    let new_x = f x in
    if D.leq new_x x then x else lfp' f new_x
  let lfp f = lfp' f D.bot

  let rec lim' (f: D.t -> D.t) (x:D.t): D.t =
    let new_x = f x in
    let widened = D.widen x new_x in
    (* print_endline ("x   " ^ D.to_string x);
    print_endline ("f x " ^ D.to_string new_x);
    print_endline ("wid " ^ D.to_string widened);
    prerr_endline ""; *)
    if D.leq widened x then x else lim' f widened
  let lim f = lim' f D.bot
end

module DDFP = Fixpoint (DD)
let rec eval_thread threads stmt t (domain:domain): domain = match stmt with
  | Assign (v, e) -> eval_assign threads v e t domain
  | Seq (s1, s2) -> eval_thread threads s2 t (eval_thread threads s1 t domain)
  | If (b, s) -> DD.join (eval_thread threads s t (eval_guard threads b true t domain)) (eval_guard threads b false t domain)
  | While (b, s) -> 
    let f envX = DD.join domain (eval_thread threads s t (eval_guard threads b true t envX)) in
    eval_guard threads b false t (DDFP.lim f)
  | Nop -> domain
  | Lock m -> eval_lock threads m t domain
  | Unlock m -> eval_unlock threads m t domain

module IntfDFP = Fixpoint (IntfD)
let rec eval (threads: stmt list) (envs:envs): domain =
  let threads_i = List.mapi (fun t thread -> t) threads in
  let f' intf =
    let ret = List.mapi (fun t thread ->
      let ret = eval_thread threads_i thread t (envs, intf) in
      print_endline ("thread " ^ string_of_int t ^ ": " ^ DD.to_string ret);
      ret
    ) threads
    |> List.fold_left DD.join DD.bot
    in
    print_endline "";
    ret
  in
  let f intf = snd (f' intf) in
  f' (IntfDFP.lim f) (* extra f' to compute envs after last intfs *)

  (* final env overapproximates from initial env *)
  (* par_ex01: x->[0, 1] not x->[1, 1] because x->[0, 0] joined from thread 1 initial env *)
  (* par_ex_fig9b: y->[0, inf] not y->[1, inf] because y->[0, 0] joined from thread 1 initial env *)