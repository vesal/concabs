module type DOM =
sig
  type t
  val to_string: t -> string
  val top: t
  val bot: t
  val join: t -> t -> t
  val leq: t -> t -> bool
  val widen: t -> t -> t
  val narrow: t -> t -> t
end

module BoolDom = 
struct
  type t = Bbot | Btv of bool | Btop
  let of_bool tv: t = Btv tv
  let top = Btop
  let bot = Bbot
  let join b1 b2 = match (b1,b2) with
    | (Bbot, x) | (x, Bbot) -> x
    | (Btv tv1, Btv tv2) when tv1 = tv2 -> b1
    | _ -> Btop
  let leq b1 b2: bool = match (b1,b2) with
    | (Bbot, _) | (_, Btop) -> true
    | _ -> b1 = b2
  let widen b1 b2 = b2
  let narrow b1 b2 = b2
end

module IntDom = 
struct
  type t = int * int

  let bot = (max_int, min_int)
  let to_string (lo,hi) =
    if (lo, hi) = bot then
      "[bot]"
    else
      let s i =
        if i = min_int then
          "-inf"
        else if i = max_int then
          "inf"
        else
          string_of_int i
      in
      "[" ^ s lo ^ ", " ^ s hi ^ "]"
  let top = (min_int, max_int)
  let join (lo1,hi1) (lo2,hi2) = (min lo1 lo2, max hi1 hi2)
  let leq (lo1,hi1) (lo2,hi2): bool = lo2 <= lo1 && hi1 <= hi2
  let widen (lo1,hi1) (lo2,hi2): t =
    if (lo1, hi1) <> bot then (
      (if lo2 < lo1 then min_int else lo1),
      (if hi1 < hi2 then max_int else hi2))
    else (* unneeded with proper (lift)bot *)
      (lo2, hi2)

  let narrow (lo1,hi1) (lo2,hi2): t = (
    (if lo1 = min_int then lo2 else lo1), 
    (if hi1 = max_int then hi2 else hi1))
  let of_int n: t = (n,n)

  (* arithmetic operations *)
  let add_op (lo1,hi1) (lo2,hi2) =
    (* TODO: check for overflow *)
    let lo = if lo1 = min_int || lo2 = min_int then min_int else lo1 + lo2 in
    let hi = if hi1 = max_int || hi2 = max_int then max_int else hi1 + hi2 in
    (lo, hi)

  (* comparison operators *)
  let leq_op (lo1,hi1) (lo2,hi2): BoolDom.t = 
    if hi1 <= lo2 then Btv true
    else if hi2 < lo1 then Btv false
    else Btop

end


let vars = ["x"; "y"; "z"]

module VarMap (D: DOM) = 
struct
  module SMap = Map.Make(String)
  type t = D.t SMap.t

  let get k m: D.t = SMap.find k m
  let updt k v m: t = SMap.add k v m
  let of_fun f: t = List.fold_left (fun m v -> SMap.add v (f v) m) SMap.empty vars
  let map2 f ht1 ht2 = of_fun (fun v -> f  (SMap.find v ht1) (SMap.find v ht2))

  let to_string m: string = String.concat ", " (List.map (fun v -> v ^ "->" ^ D.to_string (SMap.find v m)) vars)
  let bot: t = of_fun (fun v -> D.bot)
  let top: t  = of_fun (fun v -> D.top)
  let join: t -> t -> t = map2 D.join
  let widen = map2 D.widen
  let narrow = map2 D.narrow
  let leq ht1 ht2: bool = List.for_all (fun v -> D.leq (SMap.find v ht1) (SMap.find v ht2)) vars
end

let threads = [0; 1]

let list_prod l1 l2 = List.concat (List.map (fun x1 -> List.map (fun x2 -> (x1, x2)) l2) l1)
let thread_vars = list_prod threads vars

(* TODO: don't duplicate VarMap *)
module IntfMap (D: DOM) =
struct
  module TVOrdered =
  struct
    type t = int * string
    let compare = Pervasives.compare
  end
  module TVMap = Map.Make(TVOrdered)
  type t = D.t TVMap.t

  let get k m: D.t = TVMap.find k m
  let updt k v m: t = TVMap.add k v m
  let of_fun f: t = List.fold_left (fun m v -> TVMap.add v (f v) m) TVMap.empty thread_vars
  let map2 f ht1 ht2 = of_fun (fun v -> f  (TVMap.find v ht1) (TVMap.find v ht2))

  let to_string m: string = String.concat ", " (List.map (fun (t, v) -> "(" ^ string_of_int t ^ ", " ^ v ^ ")->" ^ D.to_string (TVMap.find (t, v) m)) thread_vars)
  let bot: t = of_fun (fun v -> D.bot)
  let top: t  = of_fun (fun v -> D.top)
  let join: t -> t -> t = map2 D.join
  let widen = map2 D.widen
  let narrow = map2 D.narrow
  let leq ht1 ht2: bool = List.for_all (fun v -> D.leq (TVMap.find v ht1) (TVMap.find v ht2)) thread_vars
end

module LiftBot (D: DOM) =
struct
  type t = [
    | `Bot
    | `Lift of D.t
  ]

  let to_string = function
    | `Bot -> "bot"
    | `Lift x -> "lift " ^ D.to_string x
  let top = `Lift D.top
  let bot = `Bot
  let join x y = match x, y with
    | `Lift x, `Lift y -> `Lift (D.join x y)
    | `Bot, x | x, `Bot -> x
  let leq x y = match x, y with
    | `Bot, _ -> true
    | _, `Bot -> false
    | `Lift x, `Lift y -> D.leq x y
  let widen x y = match x, y with
    | `Lift x, `Lift y -> `Lift (D.widen x y)
    | `Bot, x | x, `Bot -> x
  let narrow x y = match x, y with
    | `Bot, x | x, `Bot -> `Bot (* is this right? *)
    | `Lift x, `Lift y -> `Lift (D.narrow x y)

  let lift f = function
    | `Bot -> `Bot
    | `Lift x -> `Lift (f x)

  let to_t = function
    | `Bot -> failwith "to_t: bot"
    | `Lift x -> x
end

module Prod (D1: DOM) (D2: DOM) =
struct
  type t = D1.t * D2.t

  let to_string (x1, x2) = "(" ^ D1.to_string x1 ^ ", " ^ D2.to_string x2 ^ ")"
  let top = (D1.top, D2.top)
  let bot = (D1.bot, D2.bot)
  let join (x1, x2) (y1, y2) = (D1.join x1 y1, D2.join x2 y2)
  let leq (x1, x2) (y1, y2) = D1.leq x1 y1 && D2.leq x2 y2
  let widen (x1, x2) (y1, y2) = (D1.widen x1 y1, D2.widen x2 y2)
  let narrow (x1, x2) (y1, y2) = (D1.narrow x1 y1, D2.narrow x2 y2)
end