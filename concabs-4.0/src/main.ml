open While

let example = par_ex_fig9b

let () =
  let abs = Absint.eval example Absint.initenv in
  print_endline ("Abstract value: " ^ Absint.string_of_domain abs)

(* let () =
  print_endline (Absint.MDB.(to_string (widen bot Absint.initenv))) *)

(* let () =
  print_endline (Domains.IntDom.(to_string (widen bot (0, 0)))) *)

(* let () =
  print_endline (Domains.IntDom.(to_string (add_op (0, 1) (1, max_int)))) *)