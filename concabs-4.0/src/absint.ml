open While

module ID = Domains.IntDom
module MD = Domains.VarMap (ID)
module MDB = Domains.LiftBot (MD)
module IntfD = Domains.IntfMap (ID)
module DD = Domains.Prod (MDB) (IntfD)

type env = MDB.t
type intf = IntfD.t
type domain = DD.t

let initenv: env = `Lift (MD.of_fun (fun _ -> ID.of_int 0))
let string_of_domain (domain:domain): string = DD.to_string domain
let rec eval_aexp aexp (env:env) = match aexp with
  | Var v -> MDB.to_t (MDB.lift (MD.get v) env)
  | Intv (lo, hi) -> (lo, hi)
  | Add (e1, e2) -> ID.add_op (eval_aexp e1 env) (eval_aexp e2 env)

let get v (env:env): ID.t = MDB.to_t (MDB.lift (MD.get v) env)

let as_expr (av:ID.t): aexp =
  let (l, h) = av in
  Intv (l, h)

let rec subst f e = match e with
  | Var v -> f v
  | Intv (lo, hi) -> Intv (lo, hi)
  | Add (e1, e2) -> Add (subst f e1, subst f e2)

let apply t (env:env) (intf:intf) e =
  let f v =
    let other_ts = List.filter (fun t2 -> t2 <> t) Domains.threads in
    let v_intf = List.map (fun t2 -> IntfD.get (t2, v) intf) other_ts
                 |> List.fold_left ID.join ID.bot in
    if v_intf = ID.bot then
      Var v
    else
      as_expr (ID.join v_intf (get v env))
  in
  subst f e

let eval_assign1 v e (env:env): env =
  if env = MDB.bot then (* avoid eager eval_aexp on bot *)
    MDB.bot
  else
    MDB.lift (MD.updt v (eval_aexp e env)) env

let eval_assign v e t (env, intf): domain =
  let new_env = eval_assign1 v (apply t env intf e) env in
  let new_intf = IntfD.updt (t, v) (ID.join (IntfD.get (t, v) intf) (get v new_env)) intf in
  (new_env, new_intf)

let eval_guard1 b tv (env:env): env =
  if env = MDB.bot then
    MDB.bot
  else
    match b, tv with
    | Leq0 e, true -> (* e <= 0 *)
      let (loe, hie) = eval_aexp e env in
      if loe <= 0 then match e with
        | Var x -> MDB.lift (MD.updt x (loe, 0)) env
        | _ -> env
      else
        MDB.bot
    | Leq0 e, false -> (* !(e <= 0) => e > 0 *)
      let (loe, hie) = eval_aexp e env in
      if hie > 0 then match e with
        | Var x -> MDB.lift (MD.updt x (1, hie)) env
        | _ -> env
      else
        MDB.bot

let eval_guard b tv t (env, intf): domain = match b with
  | Leq0 e -> (eval_guard1 (Leq0 (apply t env intf e)) tv env, intf)

(* let rec lfp (f: env -> env) (env:env): env =
  let new_env = f env in
  if MDB.leq new_env env then env else lfp f new_env *)

let rec lim (f: domain -> domain) (domain:domain): domain =
  let new_domain = f domain in
  let widened = DD.widen domain new_domain in
  if DD.leq widened domain then domain else lim f widened

let rec eval_thread stmt t (domain:domain): domain = match stmt with
  | Assign (v, e) -> eval_assign v e t domain
  | Seq (s1, s2) -> eval_thread s2 t (eval_thread s1 t domain)
  | If (b, s) -> DD.join (eval_thread s t (eval_guard b true t domain)) (eval_guard b false t domain)
  | While (b, s) -> 
    let f envX = DD.join domain (eval_thread s t (eval_guard b true t envX)) in
    eval_guard b false t (lim f DD.bot)
  | Nop -> domain

let rec fix_domain (f: domain -> domain) (domain:domain): domain =
  let new_domain = f domain in
  if DD.leq new_domain domain then domain else fix_domain f new_domain

let rec eval (threads: stmt list) (env:env): domain =
  let f (_, intf) =
    let (new_env, new_intf) = List.mapi (fun t thread -> eval_thread thread t (env, intf)) threads
      |> List.fold_left DD.join DD.bot
    in
    (new_env, IntfD.widen intf new_intf) (* ignore previous env *)
  in
  f (fix_domain f DD.bot) (* extra f to compute envs after last intfs *)

  (* final env overapproximates from initial env *)
  (* par_ex01: x->[0, 1] not x->[1, 1] because x->[0, 0] joined from thread 1 initial env *)
  (* par_ex_fig9b: y->[0, inf] not y->[1, inf] because y->[0, 0] joined from thread 1 initial env *)