module type Key =
sig
  include Map.OrderedType
  val to_string: t -> string
end


module String =
struct
  include String
  let to_string s = s
end

module Int =
struct
  type t = int
  let compare = Pervasives.compare
  let to_string = string_of_int
end

module Prod (K1: Key) (K2: Key) =
struct
  type t = K1.t * K2.t
  let compare = Pervasives.compare (* should actually combine K1.compare and K2.compare ? *)
  let to_string (x1, x2) = "(" ^ K1.to_string x1 ^ ", " ^ K2.to_string x2 ^ ")"
end