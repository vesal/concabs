open While

module ID = Domains.IntDom
module IDB = Domains.LiftBot (ID)
module MD = Domains.VarMap (ID)
module MDB = Domains.LiftBotMap (MD)
module IntfD = Domains.IntfMap (IDB)
module DD = Domains.Prod (MDB) (IntfD)

type env = MDB.t
type intf = IntfD.t
type domain = DD.t

let vars = ["x"; "y"; "z"]
let initenv: env =
  vars
  |> List.map (fun k -> (k, ID.of_int 0))
  |> MDB.of_pairs
let string_of_domain (domain:domain): string = DD.to_string domain
let rec eval_aexp aexp (env:env): IDB.t = match aexp with
  | Var v -> MDB.get v env
  | Intv (lo, hi) -> `Lift (lo, hi)
  | Add (e1, e2) -> IDB.map2 ID.add_op (eval_aexp e1 env) (eval_aexp e2 env)

let get v (env:env): IDB.t = MDB.get v env

let as_expr (av:ID.t): aexp =
  let (l, h) = av in
  Intv (l, h)

let rec subst f e = match e with
  | Var v -> f v
  | Intv (lo, hi) -> Intv (lo, hi)
  | Add (e1, e2) -> Add (subst f e1, subst f e2)

let apply threads t (env:env) (intf:intf) e =
  let f v =
    let other_ts = List.filter (fun t2 -> t2 <> t) threads in
    let v_intf = List.map (fun t2 -> IntfD.get (t2, v) intf) other_ts
                 |> List.fold_left IDB.join IDB.bot in
    if v_intf = IDB.bot then
      Var v
    else
      as_expr (IDB.unlift (IDB.join v_intf (get v env))) (* safe unlift since v_intf <> bot *)
  in
  subst f e

let eval_assign1 v e (env:env): env =
  eval_aexp e env
  |> IDB.flat_map (fun x -> MDB.updt v x env)

let eval_assign threads v e t (env, intf): domain =
  let new_env = eval_assign1 v (apply threads t env intf e) env in
  let new_intf = IntfD.updt (t, v) (IDB.join (IntfD.get (t, v) intf) (get v new_env)) intf in
  (new_env, new_intf)

let eval_guard1 b tv (env:env): env = match b with
  | Leq0 e ->
    eval_aexp e env
    |> IDB.flat_map (fun (loe, hie) ->
      match tv with
      | true when loe <= 0 -> begin
        match e with
        | Var x -> MDB.updt x (loe, 0) env
        | _ -> env
        end
      | false when hie > 0 -> begin
        match e with
        | Var x -> MDB.updt x (1, hie) env
        | _ -> env
        end
      | _ -> MDB.bot
    )

let eval_guard threads b tv t (env, intf): domain = match b with
  | Leq0 e -> (eval_guard1 (Leq0 (apply threads t env intf e)) tv env, intf)


module Fixpoint (D: Domains.DOM) =
struct
  let rec lfp' (f: D.t -> D.t) (x:D.t): D.t =
    let new_x = f x in
    if D.leq new_x x then x else lfp' f new_x
  let lfp f = lfp' f D.bot

  let rec lim' (f: D.t -> D.t) (x:D.t): D.t =
    let new_x = f x in
    let widened = D.widen x new_x in
    if D.leq widened x then x else lim' f widened
  let lim f = lim' f D.bot
end

module DDFP = Fixpoint (DD)
let rec eval_thread threads stmt t (domain:domain): domain = match stmt with
  | Assign (v, e) -> eval_assign threads v e t domain
  | Seq (s1, s2) -> eval_thread threads s2 t (eval_thread threads s1 t domain)
  | If (b, s) -> DD.join (eval_thread threads s t (eval_guard threads b true t domain)) (eval_guard threads b false t domain)
  | While (b, s) -> 
    let f envX = DD.join domain (eval_thread threads s t (eval_guard threads b true t envX)) in
    eval_guard threads b false t (DDFP.lim f)
  | Nop -> domain

module IntfDFP = Fixpoint (IntfD)
let rec eval (threads: stmt list) (env:env): domain =
  let threads_i = List.mapi (fun t thread -> t) threads in
  let f' intf =
    List.mapi (fun t thread -> eval_thread threads_i thread t (env, intf)) threads
    |> List.fold_left DD.join DD.bot
  in
  let f intf = snd (f' intf) in
  f' (IntfDFP.lim f) (* extra f' to compute envs after last intfs *)

  (* final env overapproximates from initial env *)
  (* par_ex01: x->[0, 1] not x->[1, 1] because x->[0, 0] joined from thread 1 initial env *)
  (* par_ex_fig9b: y->[0, inf] not y->[1, inf] because y->[0, 0] joined from thread 1 initial env *)