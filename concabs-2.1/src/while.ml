type aexp = 
  | Lit of int
  | Add of aexp * aexp
  | Var of string

type bexp = 
  | Leq of aexp * aexp
  | Tv of bool
  | Choice

type stmt = 
  | Assign of string * aexp    (* v := e *)
  | If of bexp * stmt * stmt   (* if b then s1 else s2 *)
  | While of bexp * stmt       (* while b do s *)
  | Seq of stmt * stmt         (* s1; s2 *)
  | Nop                        (* ; *)

(* x := 3 + 10 *)
let ex01 = Assign ("x", Add (Lit 3, Lit 10))  

(* y := 5; x := 2 + y *)
let ex02 = Seq (Assign ("y", (Lit 5)), Assign ("x", Add (Lit 2, Var "y"))) 

(* if 2 <= 1 then ex01 else ex02 *)
let ex03 = If (Leq (Lit 2, Lit 3), ex01, ex02)

(* if ? then ex01 else ex02 *)
let ex04 = If (Choice, ex01, ex02)  

(*  while (y <= 10) { x := x + y; y := y + 1 } *)
let ex05 = 
  let check = Leq (Var "y", Lit 10) in
  let inc_y = Assign ("y", Add (Var "y", Lit 1)) in
  let add_y_to_x = Assign ("x", Add (Var "x", Var "y")) in
  While (check, Seq (add_y_to_x, inc_y))

(*  while (y <= 10) { 
 *    if (?) x := x + y; 
 *    else   x := x + x; 
 *    y := y + 1 
 *  } *)
let ex06 = 
  let check = Leq (Var "y", Lit 10) in
  let inc_y = Assign ("y", Add (Var "y", Lit 1)) in
  let add_y_to_x = Assign ("x", Add (Var "x", Var "y")) in
  let add_x_to_x = Assign ("x", Add (Var "x", Var "x")) in
  let choose = If (Choice, add_x_to_x, add_y_to_x) in
  While (check, Seq (choose, inc_y))

(*  while (?) { x := x + y; y := y + 1 } *)
let ex07 = 
  let inc_y = Assign ("y", Add (Var "y", Lit 1)) in
  let add_y_to_x = Assign ("x", Add (Var "x", Var "y")) in
  While (Choice, Seq (add_y_to_x, inc_y))

(*  if (?) y++; 
 *  if (y <= 0) y++ *)  
let ex08 = 
  let inc_x = Assign ("x", Add (Var "x", Lit 1)) in
  Seq (If (Choice, inc_x, Nop), If (Leq (Var "x", Lit 0), inc_x, Nop))

let ex09 =
  let inc_x = Assign ("x", Add (Var "x", Lit 1)) in
  While (Leq (Var "x", Lit 10), inc_x)

let example = ex09