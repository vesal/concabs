open While

type env = string -> int
let initenv x = 0
let updt (env:env) var value: env = 
  (fun v -> if v = var then value else env v) 

let rec eval_aexp env aexp: int = match aexp with 
  | Lit n -> n
  | Add (e1, e2) -> eval_aexp env e1 + eval_aexp env e2
  | Var x -> env x

let rec eval_bexp env bexp: bool = match bexp with
  | Leq (e1, e2) -> eval_aexp env e1 <= eval_aexp env e2
  | Tv b -> b
  | Choice -> Random.bool ()

let rec eval env stmt: env = match stmt with
  | Assign (v, e) -> updt env v (eval_aexp env e)
  | Seq (s1, s2) -> 
    let env1 = eval env s1 in
    eval env1 s2
  | If (b, s1, s2) -> 
    if eval_bexp env b then eval env s1 else eval env s2
  | While (b, s) ->
    if eval_bexp env b 
    then eval (eval env s) stmt
    else env
  | Nop -> env