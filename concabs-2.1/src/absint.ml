open While

module BD = Domains.BoolDom
module ID = Domains.IntDom
module MD = Domains.VarMap (ID)

type domain = ID.t
type booldomain = BD.t
type env = MD.t

let initenv: env = MD.of_fun (fun _ -> ID.of_int 0)
let string_of_env (env: env): string = MD.to_string env

let rec eval_aexp (env:env) aexp: domain = match aexp with 
  | Lit n -> ID.of_int n
  | Add (e1, e2) -> ID.add_op (eval_aexp env e1) (eval_aexp env e2)
  | Var x -> MD.get env x

let rec eval_bexp (env:env) bexp: booldomain = match bexp with
  | Leq (e1, e2) -> ID.leq_op (eval_aexp env e1) (eval_aexp env e2)
  | Tv b -> Btv b
  | Choice -> Btop

let rec restrict b tv env = match (b,tv) with
  | Leq (Var x, e), true -> 
    let (_,hi) = eval_aexp env e in
    let (lo,_) = MD.get env x in
    MD.updt env x (lo,hi) 
  | Leq (Var x, e), false -> 
    let (lo,_) = eval_aexp env e in
    let (_,hi) = MD.get env x in
    MD.updt env x (lo+1,hi) 
  | _ -> env

let rec lfp (f: env -> env) (env:env): env = 
  let new_env = f env in
  if MD.leq new_env env then env else lfp f new_env

let rec widening (f: env -> env) (env:env): env = 
  let new_env = f env in
  let widened = MD.widen env new_env in
  if MD.leq widened env then env else widening f widened

let rec narrowing (f: env -> env) (env:env): env = 
  let new_env = f env in
  let narrowed = MD.narrow env new_env in
  if MD.leq env narrowed then env else narrowing f narrowed

let rec eval (env:env) stmt: env = match stmt with
  | Assign (v,e) -> MD.updt env v (eval_aexp env e)
  | Seq (s1, s2) -> eval (eval env s1) s2
  | If (b, s1, s2) -> begin 
      match eval_bexp env b  with
      | Btv true -> eval (restrict b true env) s1
      | Btv false -> eval (restrict b false env) s2
      | Btop -> MD.join (eval (restrict b true env) s1) (eval (restrict b false env) s2)
      | Bbot -> MD.bot
    end
  | While (b, s) -> 
    let f env = match eval_bexp env b  with
      | Btv true -> eval (restrict b true env) s
      | Btv false -> restrict b false env
      | Btop -> 
        let entry = restrict b true env in
        let step = eval entry s in
        restrict b false (MD.join entry step)
      | Bbot -> MD.bot 
    in
    widening f env
  | _ -> env